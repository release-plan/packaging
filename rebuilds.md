## REBUILDS
# Note that packages only need to be rebuilt when there's an [soname](https://wikipedia.org/wiki/Soname) bump

binutils

- kernels


boost-libs

- calamares
- calamares5
- calamares-git


dav1d

- pix


gnome-desktop

- gnome-control-center
- mutter-x11-scaling


gcc

- kernels


icu

- boxit
- boxit-arm
- calamares
- calamares5
- calamares-git
- manjaro-settings-manager
- plasma-workspace (temp overlay)


hwinfo

- calamares
- calamares-git
- manjaro-settings-manager
- mhwd


evolution-data-server (libecal)

- phosh


libdisplay-info

- mutter-x11-scaling


libjxl

- pix


libphonenumber

- chatty


libtiff

- pix


libwacom

- gnome-control-center


openssl

- pacman
- systemd
- zfs-utils
- firmware-manager
- kernels
- watchmate

pacman (libalpm.so)

- pacman
- pacman-contrib
- libpamac
- pamac-cli
- pamac
- alpm-octopi-utils
- octopi
- yay

protobuf

- chatty


python (libpython3.so)

- backintime
- calamares
- calamares5
- calamares-git
- gdm-settings
- thunarx-python

python (site-packages) > Requires `manjaro-check-repos`:

  ```
  sudo mbn update
  sudo mbn --update files --unstable
  mbn files 'usr/lib/python3.XX' --unstable | grep '::'
  ```


qt5-base

- manjaro-settings-manager


qt6-base

- calamares
- octopi


qtermwidget

- octopi


yaml-cpp

- calamares
- calamares5
- calamares-git
- coreboot-configurator
